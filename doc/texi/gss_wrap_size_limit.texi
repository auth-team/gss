@subheading gss_wrap_size_limit
@anchor{gss_wrap_size_limit}
@deftypefun {OM_uint32} {gss_wrap_size_limit} (OM_uint32 * @var{minor_status}, const gss_ctx_id_t @var{context_handle}, int @var{conf_req_flag}, gss_qop_t @var{qop_req}, OM_uint32 @var{req_output_size}, OM_uint32 * @var{max_input_size})
@var{minor_status}: 
@var{context_handle}: 
@var{conf_req_flag}: 
@var{qop_req}: 
@var{req_output_size}: 
@var{max_input_size}: 
@end deftypefun

