@subheading gss_inquire_mechs_for_name
@anchor{gss_inquire_mechs_for_name}
@deftypefun {OM_uint32} {gss_inquire_mechs_for_name} (OM_uint32 * @var{minor_status}, const gss_name_t @var{input_name}, gss_OID_set * @var{mech_types})
@var{minor_status}: 
@var{input_name}: 
@var{mech_types}: 
@end deftypefun

