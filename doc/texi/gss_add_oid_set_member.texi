@subheading gss_add_oid_set_member
@anchor{gss_add_oid_set_member}
@deftypefun {OM_uint32} {gss_add_oid_set_member} (OM_uint32 * @var{minor_status}, const gss_OID @var{member_oid}, gss_OID_set * @var{oid_set})
@var{minor_status}: 
@var{member_oid}: 
@var{oid_set}: 
@end deftypefun

