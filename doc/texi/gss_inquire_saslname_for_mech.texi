@subheading gss_inquire_saslname_for_mech
@anchor{gss_inquire_saslname_for_mech}
@deftypefun {OM_uint32} {gss_inquire_saslname_for_mech} (OM_uint32 * @var{minor_status}, const gss_OID @var{desired_mech}, gss_buffer_t @var{sasl_mech_name}, gss_buffer_t @var{mech_name}, gss_buffer_t @var{mech_description})
@var{minor_status}: 
@var{desired_mech}: 
@var{sasl_mech_name}: 
@var{mech_name}: 
@var{mech_description}: 
@end deftypefun

