@subheading gss_wrap
@anchor{gss_wrap}
@deftypefun {OM_uint32} {gss_wrap} (OM_uint32 * @var{minor_status}, const gss_ctx_id_t @var{context_handle}, int @var{conf_req_flag}, gss_qop_t @var{qop_req}, const gss_buffer_t @var{input_message_buffer}, int * @var{conf_state}, gss_buffer_t @var{output_message_buffer})
@var{minor_status}: 
@var{context_handle}: 
@var{conf_req_flag}: 
@var{qop_req}: 
@var{input_message_buffer}: 
@var{conf_state}: 
@var{output_message_buffer}: 
@end deftypefun

