@subheading gss_get_mic
@anchor{gss_get_mic}
@deftypefun {OM_uint32} {gss_get_mic} (OM_uint32 * @var{minor_status}, const gss_ctx_id_t @var{context_handle}, gss_qop_t @var{qop_req}, const gss_buffer_t @var{message_buffer}, gss_buffer_t @var{message_token})
@var{minor_status}: 
@var{context_handle}: 
@var{qop_req}: 
@var{message_buffer}: 
@var{message_token}: 
@end deftypefun

