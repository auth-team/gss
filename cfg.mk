# Copyright (C) 2003-2022 Simon Josefsson
#
# This file is part of the GNU Generic Security Service Library.
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <https://www.gnu.org/licenses/>.

manual_title = Generic Security Service Library

old_NEWS_hash = a740afe77a4824914e296b217b7640a8

bootstrap-tools = autoconf,automake,libtoolize,gnulib,makeinfo,help2man,gengetopt,gtkdocize,tar,gzip

local-checks-to-skip = \
	sc_GPL_version \
	sc_prohibit_gnu_make_extensions \
	sc_prohibit_strcmp \
	sc_require_config_h \
	sc_require_config_h_first

exclude_file_name_regexp--sc_unmarked_diagnostics = ^src/gss.c
exclude_file_name_regexp--sc_bindtextdomain = ^tests/.*$$
exclude_file_name_regexp--sc_cast_of_argument_to_free = ^lib/misc.c|lib/name.c$$
exclude_file_name_regexp--sc_prohibit_cvs_keyword = ^doc/asciidoc$$
exclude_file_name_regexp--sc_prohibit_empty_lines_at_EOF = ^tests/krb5context.tkt$$
exclude_file_name_regexp--sc_prohibit_test_minus_ao = ^doc/asciidoc$$
exclude_file_name_regexp--sc_trailing_blank = ^doc/asciidoc|doc/gdoc$$

VC_LIST_ALWAYS_EXCLUDE_REGEX = ^maint.mk|GNUmakefile|gtk-doc.make|po/.*.po.in|doc/fdl-1.3.texi|doc/gendocs_template|m4/pkg.m4|build-aux/|lib/gl/.*|src/gl/.*$$

update-copyright-env = UPDATE_COPYRIGHT_HOLDER="Simon Josefsson" UPDATE_COPYRIGHT_USE_INTERVALS=2 UPDATE_COPYRIGHT_FORCE=1

review-diff:
	git diff `git describe --abbrev=0`.. \
	| grep -v -e ^index -e '^diff --git' \
	| filterdiff -p 1 -x 'build-aux/*' -x 'gl/*' -x 'lib/gl/*' -x 'src/gl/*' -x 'po/*' -x 'maint.mk' -x '.gitignore' -x '.x-sc*' -x ChangeLog -x GNUmakefile \
	| less
