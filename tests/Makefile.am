# Copyright (C) 2003-2022 Simon Josefsson
#
# This file is part of the GNU Generic Security Service Library.
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <https://www.gnu.org/licenses/>.

AM_CFLAGS = $(WARN_CFLAGS) $(WERROR_CFLAGS)
AM_CPPFLAGS = -I$(top_builddir)/lib/headers -I$(top_srcdir)/lib/headers
AM_CPPFLAGS += -DLOCALEDIR=\"$(datadir)/locale\"
AM_LDFLAGS = -no-install
LDADD = ../lib/libgss.la $(LTLIBINTL)

EXTRA_DIST = krb5context.key krb5context.tkt utils.c shishi.conf

AM_TESTS_ENVIRONMENT = \
	SHISHI_KEYS=$(srcdir)/krb5context.key \
	SHISHI_TICKETS=$(srcdir)/krb5context.tkt \
	SHISHI_CONFIG=$(srcdir)/shishi.conf \
	SHISHI_HOME=$(srcdir) \
	SHISHI_USER=ignore-this-warning \
	THREADSAFETY_FILES="$(top_srcdir)/lib/*.c $(top_srcdir)/lib/krb5/*.c" \
	EGREP="$(EGREP)"; \
	export SHISHI_KEYS SHISHI_TICKETS SHISHI_CONFIG SHISHI_HOME SHISHI_USER THREADSAFETY_FILES EGREP;

LOG_COMPILER=$(VALGRIND)

ctests = basic saslname
if KRB5
ctests += krb5context
endif

check_PROGRAMS = $(ctests)
dist_check_SCRIPTS = threadsafety.sh
TEST_EXTENSIONS = .sh
TESTS = $(dist_check_SCRIPTS) $(ctests)

krb5context_LDADD = $(LDADD) $(LTLIBSHISHI)
