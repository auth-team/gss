# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the gss package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gss 1.0.4\n"
"Report-Msgid-Bugs-To: bug-gss@gnu.org\n"
"POT-Creation-Date: 2022-08-06 15:19+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: lib/meta.c:46
msgid "Kerberos V5 GSS-API mechanism"
msgstr ""

#: lib/error.c:46
msgid "A required input parameter could not be read"
msgstr ""

#: lib/error.c:48
msgid "A required output parameter could not be written"
msgstr ""

#: lib/error.c:50
msgid "A parameter was malformed"
msgstr ""

#: lib/error.c:55
msgid "An unsupported mechanism was requested"
msgstr ""

#: lib/error.c:57
msgid "An invalid name was supplied"
msgstr ""

#: lib/error.c:59
msgid "A supplied name was of an unsupported type"
msgstr ""

#: lib/error.c:61
msgid "Incorrect channel bindings were supplied"
msgstr ""

#: lib/error.c:63
msgid "An invalid status code was supplied"
msgstr ""

#: lib/error.c:65
msgid "A token had an invalid MIC"
msgstr ""

#: lib/error.c:67
msgid ""
"No credentials were supplied, or the credentials were unavailable or "
"inaccessible"
msgstr ""

#: lib/error.c:70
msgid "No context has been established"
msgstr ""

#: lib/error.c:72
msgid "A token was invalid"
msgstr ""

#: lib/error.c:74
msgid "A credential was invalid"
msgstr ""

#: lib/error.c:76
msgid "The referenced credentials have expired"
msgstr ""

#: lib/error.c:78
msgid "The context has expired"
msgstr ""

#: lib/error.c:80
msgid "Unspecified error in underlying mechanism"
msgstr ""

#: lib/error.c:82
msgid "The quality-of-protection requested could not be provided"
msgstr ""

#: lib/error.c:84
msgid "The operation is forbidden by local security policy"
msgstr ""

#: lib/error.c:86
msgid "The operation or option is unavailable"
msgstr ""

#: lib/error.c:88
msgid "The requested credential element already exists"
msgstr ""

#: lib/error.c:90
msgid "The provided name was not a mechanism name"
msgstr ""

#: lib/error.c:95
msgid ""
"The gss_init_sec_context() or gss_accept_sec_context() function must be "
"called again to complete its function"
msgstr ""

#: lib/error.c:98
msgid "The token was a duplicate of an earlier token"
msgstr ""

#: lib/error.c:100
msgid "The token's validity period has expired"
msgstr ""

#: lib/error.c:102
msgid "A later token has already been processed"
msgstr ""

#: lib/error.c:104
msgid "An expected per-message token was not received"
msgstr ""

#: lib/error.c:321
msgid "No error"
msgstr ""

#: lib/krb5/error.c:45
msgid "No @ in SERVICE-NAME name string"
msgstr ""

#: lib/krb5/error.c:47
msgid "STRING-UID-NAME contains nondigits"
msgstr ""

#: lib/krb5/error.c:49
msgid "UID does not resolve to username"
msgstr ""

#: lib/krb5/error.c:51
msgid "Validation error"
msgstr ""

#: lib/krb5/error.c:53
msgid "Couldn't allocate gss_buffer_t data"
msgstr ""

#: lib/krb5/error.c:55
msgid "Message context invalid"
msgstr ""

#: lib/krb5/error.c:57
msgid "Buffer is the wrong size"
msgstr ""

#: lib/krb5/error.c:59
msgid "Credential usage type is unknown"
msgstr ""

#: lib/krb5/error.c:61
msgid "Unknown quality of protection specified"
msgstr ""

#: lib/krb5/error.c:64
msgid "Principal in credential cache does not match desired name"
msgstr ""

#: lib/krb5/error.c:66
msgid "No principal in keytab matches desired name"
msgstr ""

#: lib/krb5/error.c:68
msgid "Credential cache has no TGT"
msgstr ""

#: lib/krb5/error.c:70
msgid "Authenticator has no subkey"
msgstr ""

#: lib/krb5/error.c:72
msgid "Context is already fully established"
msgstr ""

#: lib/krb5/error.c:74
msgid "Unknown signature type in token"
msgstr ""

#: lib/krb5/error.c:76
msgid "Invalid field length in token"
msgstr ""

#: lib/krb5/error.c:78
msgid "Attempt to use incomplete security context"
msgstr ""

#: lib/krb5/error.c:95
msgid "No krb5 error"
msgstr ""

#: lib/krb5/error.c:136
msgid "Unknown krb5 error"
msgstr ""

#: src/gss.c:65
#, c-format
msgid "Try `%s --help' for more information.\n"
msgstr ""

#: src/gss.c:69
#, c-format
msgid "Usage: %s OPTIONS...\n"
msgstr ""

#: src/gss.c:72
msgid ""
"Command line interface to GSS, used to explain error codes.\n"
"\n"
msgstr ""

#: src/gss.c:76
msgid ""
"Mandatory arguments to long options are mandatory for short options too.\n"
msgstr ""

#: src/gss.c:79
msgid ""
"  -h, --help        Print help and exit.\n"
"  -V, --version     Print version and exit.\n"
"  -l, --list-mechanisms\n"
"                    List information about supported mechanisms\n"
"                    in a human readable format.\n"
"  -m, --major=LONG  Describe a `major status' error code value.\n"
msgstr ""

#: src/gss.c:87
msgid ""
"  -a, --accept-sec-context[=MECH]\n"
"                    Accept a security context as server.\n"
"                    If MECH is not specified, no credentials\n"
"                    will be acquired.  Use \"*\" to use library\n"
"                    default mechanism.\n"
"  -i, --init-sec-context=MECH\n"
"                    Initialize a security context as client.\n"
"                    MECH is the SASL name of mechanism, use -l\n"
"                    to list supported mechanisms.\n"
"  -n, --server-name=SERVICE@HOSTNAME\n"
"                    For -i and -a, set the name of the remote host.\n"
"                    For example, \"imap@mail.example.com\".\n"
msgstr ""

#: src/gss.c:101
msgid "  -q, --quiet       Silent operation (default=off).\n"
msgstr ""

#: src/gss.c:120
#, c-format
msgid ""
"GSS-API major status code %u (0x%x).\n"
"\n"
msgstr ""

#: src/gss.c:122
#, c-format
msgid ""
"   MSB                                                                 LSB\n"
"   +-----------------+-----------------+---------------------------------+\n"
"   |  Calling Error  |  Routine Error  |       Supplementary Info        |\n"
"   | "
msgstr ""

#: src/gss.c:136
#, c-format
msgid ""
"|\n"
"   +-----------------+-----------------+---------------------------------+\n"
"Bit 31            24  23            16  15                             0\n"
"\n"
msgstr ""

#: src/gss.c:146
#, c-format
msgid "Masked routine error %lu (0x%lx) shifted into %lu (0x%lx):\n"
msgstr ""

#: src/gss.c:162 src/gss.c:197 src/gss.c:234
#, c-format
msgid "displaying status code failed (%u/%u)"
msgstr ""

#: src/gss.c:183
#, c-format
msgid "Masked calling error %lu (0x%lx) shifted into %lu (0x%lx):\n"
msgstr ""

#: src/gss.c:217
#, c-format
msgid "Masked supplementary info %lu (0x%lx) shifted into %lu (0x%lx):\n"
msgstr ""

#: src/gss.c:252
#, c-format
msgid "No error\n"
msgstr ""

#: src/gss.c:270
#, c-format
msgid "indicating mechanisms failed (%u/%u)"
msgstr ""

#: src/gss.c:287
#, c-format
msgid "inquiring information about mechanism failed (%u/%u)"
msgstr ""

#: src/gss.c:344 src/gss.c:485
#, c-format
msgid "inquiring mechanism for SASL name (%u/%u)"
msgstr ""

#: src/gss.c:357 src/gss.c:500
#, c-format
msgid "could not import server name \"%s\" (%u/%u)"
msgstr ""

#: src/gss.c:376
#, c-format
msgid "initializing security context failed (%u/%u)"
msgstr ""

#: src/gss.c:380 src/gss.c:566
#, c-format
msgid "base64 input too long"
msgstr ""

#: src/gss.c:382 src/gss.c:417 src/gss.c:547 src/gss.c:568
#, c-format
msgid "malloc"
msgstr ""

#: src/gss.c:409 src/gss.c:539
#, c-format
msgid "getline"
msgstr ""

#: src/gss.c:411 src/gss.c:541
#, c-format
msgid "end of file"
msgstr ""

#: src/gss.c:415 src/gss.c:545
#, c-format
msgid "base64 fail"
msgstr ""

#: src/gss.c:522
#, c-format
msgid "could not acquire server credentials (%u/%u)"
msgstr ""

#: src/gss.c:562
#, c-format
msgid "accepting security context failed (%u/%u)"
msgstr ""
